<?php

/**
 * Class extending the paging_api_handler class
 * Handles previous - next objects based on Apache Solr search results
 */
class paging_api_apachesolr_handler extends paging_api_handler {
  /**
   * Create a unique fingerprint for the current state of a given View,
   * so a view in the same context will return the same fingerprint
   */
  function get_fingerprint($data) {
    $query = $data['query'];
    // Query
    $fingerprint = $query->get_query_basic();
    // Filters
    $fingerprint .= serialize($query->get_fq());
    // Sort
    $fingerprint .= serialize($query->get_solrsort());
    return md5($fingerprint);
  }

  /**
   * Format the current paging context in a way that is suitable for caching purposes
   */
  function format_context_storage($data) {
    $results = $data['results'];
    $query = $data['query'];

    // prepare this query for cache, so we retrieve it's settings
    // when we need to load the next page
    $query_data = array();
    $query_data['keys'] = $query->get_query_basic();
    $query_data['filters'] = implode(' ', $query->get_fq());
    $query_data['sort'] = implode(' ', $query->get_solrsort());
    return $query_data;
  }

  /**
   * Get an array of object ids for the current page (most likely node IDs)
   */
  function get_object_ids($data) {
    $results = $data['results'];
    // Loop over results on this page, and store the nids appropriately
    $entries = array();
    if ($results) {
      foreach ($results as $result) {
        $entries[] = $result['fields']['nid']['value'];
      }
    }
    return $entries;
  }

  /**
   * Implements the special case where we need the next object ID from a page
   * that isn't cached yet
   */
  function get_next_on_new_page($state_id, $page_id) {
    global $paging_api_rerender;
    // get current state's view out of cache
    if ($cache = cache_get($this->get_module() . '_' . $state_id, 'cache_paging_api')) {
      $querydata = $cache->data;
    }
    if ($querydata) {
      $paging_api_rerender = 1;
      $results = apachesolr_search_execute($querydata['keys'], $querydata['filters'], $querydata['sort'], 'search/apachesolr_search', $page_id);
      // Loop over results on this page, and store the nids appropriately
      $entries = array();
      if ($results) {
        foreach ($results as $result) {
          $entries[$page_id + 1][] = $result['fields']['nid']['value'];
        }
        $this->store_entries($state_id, $entries, $page_id + 1);
      }

      // re-executing the query should have updated state at this point
      $new_data = $this->get_state($state_id);
      if (isset($new_data[$page_id + 1][0])) {
        return $new_data[$page_id + 1][0];
      }
      return NULL;
    }
  }
}
