<?php

/**
 * @file
 * Renders administrative pages for Paging API module
 */

function paging_api_admin_settings() {
  $form = array();
  $form['paging_api_anonymous_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Paging API caching settings (advanced)'),
    '#description' => t('Advanced settings to make Paging API work for anonymous users & cached pages'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['paging_api_anonymous_settings']['paging_api_anonymous'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable anonymous paging on cached pages (advanced)'),
    '#default_value' => variable_get('paging_api_anonymous', FALSE),
  );

  $form['paging_api_anonymous_settings']['paging_api_anonymous_selector'] = array(
    '#type' => 'textfield',
    '#title' => t('jQuery selector'),
    '#description' => t('Specify the jQuery selector where the AJAX paging block should be appended to'),
    '#required' => FALSE,
    '#size' => 60,
    '#maxlength' => 128,
    '#default_value' => variable_get('paging_api_anonymous_selector', '#content'),
  );
  return system_settings_form($form);
}