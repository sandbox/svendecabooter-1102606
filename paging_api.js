$(document).ready(function() {
  var callback_url = Drupal.settings.paging_api.paging_api_component;
  // alert(callback_url);
  $.get(callback_url, function(data) {
    var selector = Drupal.settings.paging_api.paging_api_selector;
    $(selector).append(data);
  });
});