<?php

abstract class paging_api_handler {
  private $module;
  private $current_object_page; // page ID (index starting at 1)

  /**
   * Class contructor
   */
  function __construct($module) {
    $this->set_module($module);
  }

###############################
# Getters & setters
###############################

  /**
   * Retrieve the module name.
   */
  function get_module() {
    return $this->module;
  }

  /**
   * Set the module name.
   */
  function set_module($module) {
    $this->module = $module;
  }

  /**
   * Retrieve the current object page
   */
  function get_current_object_page() {
    return $this->current_object_page;
  }

  /**
   * Set the current object page
   */
  function set_current_object_page($pageid) {
    $this->current_object_page = $pageid;
  }

###############################
# Abstract methods
###############################

  /**
   * Create a unique fingerprint for the current state of a given object,
   * so an object in the same context will return the same fingerprint
   */
  abstract function get_fingerprint($object);

  /**
   * Format the current paging context in a way that is suitable for caching purposes
   */
  abstract function format_context_storage($context);

  /**
   * Get an array of object ids for the current page (most likely node IDs)
   */
  abstract function get_object_ids($context);

  /**
   * Implements the special case where we need the next object ID from a page
   * that isn't cached yet
   */
  abstract function get_next_on_new_page($state_id, $page_id);


###############################
# General Paging API methods
###############################

  /**
   * Get the paging state for a given paging context,
   * identified by a unique identifier
   */
  function get_state($state_id) {
    if ($cache = cache_get($state_id, 'cache_paging_api')){
      return $cache->data;
    }
  }

  /**
   * Store the current paging state for a given paging context,
   * identified by a unique identifier
   */
  function save_state($state_id, $data) {
    cache_set($state_id, $data, 'cache_paging_api', time() + (60 * 60));
  }

  /**
   * Clean up paging state
   */
  function delete_state($state_id) {
    cache_clear_all($state_id, 'cache_paging_api');
  }

  /**
   * Render the state ID based on the fingerprint of the current context
   * and a random hash to keep states unique per user
   */
  protected function render_state_id($class, $data) {
    $state_id = NULL;
    $stateinfo = paging_api_get_active_state_info();
    // Check if the current active state is for this object.
    if ($stateinfo['module'] == $this->get_module()) {
        $active_state_id = $stateinfo['state_id'];
        list ($active_fingerprint, $active_hash) = explode('--', $active_state_id);
        $new_fingerprint = $this->get_fingerprint($data);
        if ($active_fingerprint == $new_fingerprint) {
          $state_id = $stateinfo['state_id'];
        }
      }
      // If we can't reuse the state, create a new one
      if (!isset($state_id)) {
        $state_id = $this->get_fingerprint($data) . '--' . user_password(5);
      }
      return $state_id;
  }

  /**
   * Retrieve the overview URL
   */
  function get_overview_url() {
    if (isset($_SESSION['paging_api_overview_url'])) {
      return $_SESSION['paging_api_overview_url'];
    }
  }

  /**
   * Set the overview URL
   */
  function set_overview_url($url) {
    $_SESSION['paging_api_overview_url'] = $url;
  }

  /**
   * Process the data and save the structured object order in the state
   */
  function process_entries($context) {
    // get the current state ID, based on the fingerprint of the current context
    $state_id = $this->render_state_id($this, $context);

    // store the current overview URL pattern
    $this->store_overview_url($context);

    // prepare the current context for storage in the state (cache)
    $data = $this->format_context_storage($context);
    cache_set($this->get_module() . '_' . $state_id, $data, 'cache_paging_api', time() + (60 * 60));

    // set the active state info: makes sure to remember what state
    // to use for paging during following page loads
    paging_api_set_active_state_info($this->get_module(), $state_id);

    // get the current page id
    $current_page = $this->get_current_page($context);

    $entries[$current_page] = $this->get_object_ids($context);

    $this->store_entries($state_id, $entries, $current_page);
  }

  /**
   * Store a growing array of entries IDs for the current state
   */
  protected function store_entries($state_id, $entries, $pageid) {
    // merge together entries from previous pages with current into the state
    $current_state = $this->get_state($state_id);
    if (!isset($current_state[$pageid])) {
      if (is_array($current_state)) {
        $current_state[$pageid] = $entries[$pageid];
      }
      else {
        $current_state = $entries;
      }

      // save the state - this stores nid order for this page
      // and previous pages that where already stored in the state previously
      $this->save_state($state_id, $current_state);
    }
  }

  /**
   * Get the current page ID - index starts at 1
   */
  protected function get_current_page($context) {
    global $pager_page_array;
    //global $pager_total;
    if (isset($pager_page_array[0])) {
      $current_page = $pager_page_array[0] + 1; // start at index 1
      //$total_pages = $pager_total[0] + 1;
    }
    else {
      $current_page = 1;
      //$total_pages = 1;
    }
    return $current_page;
  }

  /**
   * Store the overview URL, stripped from paging variables
   * @TODO: this could probably be done better...
   */
  function store_overview_url($context) {
    global $paging_api_rerender;
    // provide a way out to not rerender overview url
    if (is_object($context) && isset($context->paging_api_rerender)) {
      return;
    }
    if ($paging_api_rerender == 1) {
      return;
    }
    
    $overview_url = $_GET;
    if (isset($overview_url['page'])) {
      unset($overview_url['page']);
    }
    $this->set_overview_url($overview_url);
  }

  /**
   * Render the overview URL based on the context the user is paging in
   * This will return the user to the page the currently viewed object is on
   */
  function render_overview_url() {
    if ($url = $this->get_overview_url()) {
      $current_page = $this->get_current_object_page();
      if ($current_page) {
        $q = $url['q'];
        unset($url['q']);
        $query = $url;
        $query['page'] = $current_page - 1; // use Drupal 0-based index
        return array(
          'url' => $q,
          'query' => $query,
        );
      }
    }
  }

  /**
   * Get a pager array, containing the previous and next object ID
   */
  function get_pager($state_id, $object_id) {
    // get state data
    $data = $this->get_state($state_id);
    if ($data) {
      foreach ($data as $page_id => $objects) {
        foreach ($objects as $position => $object) {
          if ($object_id == $object) {
            // we found the position - store it
            $this->set_current_object_page($page_id);
            return array(
              'prev' => $this->get_prev($data, $position, $state_id),
              'next' => $this->get_next($data, $position, $state_id),
            );
          }
        }
      }
    }
    return NULL;
  }

  /**
   * Get the prev object item, given the specific state and the current object position
   */
  private function get_prev($data, $current_position, $state_id) {
    $current_object_pg = $this->get_current_object_page();
    if ($current_position != 0) {
      return $data[$current_object_pg][$current_position - 1];
    }
    else {
      // special case: this is the first item in this array
      // get it from the previous page
      if ($current_object_pg != 1) { // check if we're not on the first spot anyway
        $previous_page = $current_object_pg - 1;
        $num_objects_on_previous_page = count($data[$previous_page]);
        $idx = $num_objects_on_previous_page - 1;
        return $data[$previous_page][$idx];
      }
    }
    return NULL;
  }

  /**
   * Get the next object item, given the specific state and the current object position
   */
  private function get_next($data, $current_position, $state_id) {
    $current_object_pg = $this->get_current_object_page();
    if ($current_position != (count($data[$current_object_pg]) - 1)) { // not at last position
      return $data[$current_object_pg][$current_position + 1];
    }
    else {
      // special case: this is the last item in this array
      // check if the next page is already in the state cache
      if (isset($data[$current_object_pg + 1][0])) {
        // return the first objectid from the next page
        return $data[$current_object_pg + 1][0];
      }
      else { // update state so next page is in there
        return $this->get_next_on_new_page($state_id, $current_object_pg);
      }
    }
    return NULL;
  }
}