Description
============
General info here

Installation
=============

Advanced usage
===============

Use paging api paging block for anonymous users when page caching is enabled
-----------------------------------------------------------------------------
When Drupal's page caching is enabled, the paging API paging block will not show
the desired results, since the whole page HTML output is being cached
in the cache_page database table. This module can work around that by fetching
the Paging API paging block through an AJAX request.

Note:
For this to work, you need to:
- Download and enable the Component module (http://drupal.org/project/component)
- Activate this feature at admin/settings/paging_api
- Make sure the Paging API pager block is disabled at admin/build/blocks,
  since it will be fetched via AJAX
- Be aware that all pages that set the paging context (e.g. views, apache solr, ...)
  will NOT be cached anymore, since we obviously need to be able to properly set
  the right paging context for each anonymous user individually.
  You should be aware of the performance implication of this, certainly if a lot
  of contexts are activated.