<?php

include_once 'paging_api_handler.inc';

/**
 * Implementation of hook_init().
 */
function paging_api_init() {
  $cache_settings = variable_get('cache', CACHE_DISABLED);
  if (user_is_anonymous() && ($cache_settings == CACHE_NORMAL || $cache_settings == CACHE_AGGRESSIVE)) {
    // If the user is anonymous, and page caching is enabled, we will have to
    // load the paging api block through javascript
    drupal_add_js(array('paging_api' => array('anonymous' => 1)), 'setting');
  }

  if (arg(0) == 'node') {
    if (variable_get('paging_api_anonymous', FALSE) == TRUE && module_exists('component')) {
      $paging_api_block = url('component/block/paging_api/paging_api_pager', array('query' => drupal_get_destination()));
      $paging_api_selector = variable_get('paging_api_anonymous_selector', '#content');
      drupal_add_js(array('paging_api' => array('paging_api_component' => $paging_api_block, 'paging_api_selector' => $paging_api_selector)), 'setting');
      drupal_add_js(drupal_get_path('module', 'paging_api') . '/paging_api.js');
    }
  }
}

/**
 * Implementation of hook_menu().
 */
function paging_api_menu() {
  $items = array();

  $items['admin/settings/paging_api'] = array(
    'title' => 'Paging API settings',
    'description' => 'Paging API settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('paging_api_admin_settings'),
    'access callback' => 'user_access',
    'access arguments' => array('administer paging api'),
    'file' => 'paging_api.admin.inc',
  );

  $items['admin/settings/paging_api/overview'] = array(
    'title' => 'Paging API settings',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );

  return $items;
}

/**
 * Implementation of hook_perm().
 */
function paging_api_perm() {
  return array('administer paging api');
}

/**
 * Implementation of hook_autoload_info().
 */
function paging_api_autoload_info() {
  $classes = array();
  $implementers = module_invoke_all('paging_api_info');
  foreach ($implementers as $module => $info) {
    $classname = $info['class'];
    $classes[$classname] = array(
      'file' => $classname . '.inc',
      'file path' => drupal_get_path('module', $module),
    );
  }
  return $classes;
}


/**
 * Implementation of hook_block().
 */
function paging_api_block($op = 'list', $delta = 0, $edit = array()) {
if ($op == 'list') {
    $blocks['paging_api_pager']['info'] = t('Paging API pager');
    $blocks['paging_api_pager']['cache'] = BLOCK_NO_CACHE;
    return $blocks;
  }
  else if ($op == 'view') {
    switch ($delta) {
      case 'paging_api_pager':
        $block = array(
          'subject' => NULL,
          'content' => paging_api_block_render_pager(),
        );
        break;
    }
    return $block;
  }
}

/**
 * Implementation of hook_theme().
 */
function paging_api_theme() {
  return array(
    'paging_api_pager' => array(
      'arguments' => array(
        'pager' => NULL,
        'overview_url' => NULL,
      ),
    ),
  );
}

/**
 * Render a pager to be displayed on an object detail page
 */
function paging_api_block_render_pager() {
  // @TODO: make more generic, so it also works on other objects
  // by calling specific method on the class that returns if context is correct?
  if (arg(0) == "component") {
    if (isset($_GET['destination'])) {
      $url = urldecode($_GET['destination']);
      $url_parts = explode('/', $url);
      if ($url_parts[0] == 'node' && is_numeric($url_parts[1])) {
        $node = node_load($url_parts[1]);
      }
    }
  }
  else {
    $node = menu_get_object();
  }
  if ($_GET['noblock'] != 1 && $node->nid) {
    $active_state_info = paging_api_get_active_state_info();
    $state_id = $active_state_info['state_id'];
    $module = $active_state_info['module'];

    $class = paging_api_get_class($module);
    if ($class) {
      $items = array();
      $pager = $class->get_pager($state_id, $node->nid);
      $overview_url = $class->render_overview_url();
      return theme('paging_api_pager', $pager, $overview_url);
    }
  }
}

/**
 * Get the paging_api implementing class for a certain module
 */
function paging_api_get_class($module) {
  $paging_api_info = module_invoke($module, 'paging_api_info');
  $classname = $paging_api_info[$module]['class'];
  if (isset($classname) && class_exists($classname)) {
    $class = new $classname($module);
    if (is_a($class, 'paging_api_handler')) {
      return $class;
    }
  }
  return NULL;
}

/**
  * Set the currently active state
  */
function paging_api_set_active_state_info($module, $state_id) {
  $_SESSION['paging_api_active_state_info'] = array(
    'module' => $module,
    'state_id' => $state_id,
  );
}

/**
  * Get the currently active state
  */
function paging_api_get_active_state_info() {
  return $_SESSION['paging_api_active_state_info'];
}

/**
 * Themeable function to render pager on object detail page
 */
function theme_paging_api_pager($pager, $overview_url) {
  if (isset($pager['prev'])) {
    $items[] = l(t('Previous'), 'node/' . $pager['prev']);
  }
  if (isset($overview_url)) {
    $items[] = l(t('List'), $overview_url['url'], array('query' => $overview_url['query']));
  }
  if (isset($pager['next'])) {
    $items[] = l(t('Next'), 'node/' . $pager['next']);
  }
  return theme('item_list', $items);
}

/**
 * Implementation of hook_flush_caches().
 */
function paging_api_flush_caches() {
  return array('cache_paging_api');
}