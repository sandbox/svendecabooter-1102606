<?php

/**
 * Class extending the paging_api_handler class
 * Handles previous - next objects based on Views
 */
class paging_api_views_handler extends paging_api_handler {
  /**
   * Create a unique fingerprint for the current state of a given View,
   * so a view in the same context will return the same fingerprint
   */
  function get_fingerprint($view) {
    $fingerprint = $view->name;
    $fingerprint .= implode('', $view->args);
    return md5($fingerprint);
  }

  /**
   * Format the current paging context in a way that is suitable for caching purposes
   */
  function format_context_storage($view) {
    $data = array();
    $data['name'] = $view->name;
    $data['args'] = $view->args;
    $data['display'] = $view->current_display;
    return $data;
  }

  /**
   * Get an array of object ids for the current page (most likely node IDs)
   */
  function get_object_ids($view) {
    // Loop over results on this page, and store the nids appropriately
    $entries = array();
    foreach ($view->result as $item) {
      $entries[] = $item->nid;
    }
    return $entries;
  }

  /**
   * Override default get_current_page
   *
   * Get the current page ID - index starts at 1
   */
  protected function get_current_page($view) {
    if ($view->pager['use_pager']) {
      $current_page = $view->pager['current_page'] + 1; // start at index 1
      //$total_pages = ceil($view->total_rows / $view->pager['items_per_page']);
    }
    else {
      $current_page = 1;
      //$total_pages = 1;
    }
    return $current_page;
  }

  /**
   * Implements the special case where we need the next object ID from a page
   * that isn't cached yet
   */
  function get_next_on_new_page($state_id, $page_id) {
    // get current state's view out of cache
    if ($cache = cache_get($this->get_module() . '_' . $state_id, 'cache_paging_api')) {
      $view_data = $cache->data;
    }
    
    if ($view_data) {
      // render next page for current state's view, so we can get the next nids
      $view = views_get_view($view_data['name']);
      if (is_object($view)) {
        $view->pager['current_page'] = $page_id;
        global $pager_page_array;
        $pager_page_array[$view->pager['element']] = $page_id;
        // add indication that we are rerendering an existing view
        $view->paging_api_rerender = TRUE;
        // re-render the view for the next page
        if (method_exists($view, 'preview')) {
          $view->preview($view_data['display'], $view_data['args']);
        }
      }
    }

    // re-rendering the view should have updated state at this point
    $new_data = $this->get_state($state_id);
    if (isset($new_data[$page_id + 1][0])) {
      return $new_data[$page_id + 1][0];
    }
    return NULL;
  }
}