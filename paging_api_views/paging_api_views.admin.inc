<?php

/**
 * @file
 * Renders administrative pages for paging_api_views module
 */

/**
 * Form for paging_api_views administrative settings
 */
function paging_api_views_admin_settings() {
  $form = array();
  $views = array();
  foreach (views_get_all_views() as $key => $view) {
    $views[$key] = $view->name;
  }

  $form['paging_api_views_enabled_views'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Enabled on these views'),
    '#weight' => 0,
    '#description' => t('Specify the views where the Paging API Views functionality should be enabled on.'),
    '#options' => $views,
    '#default_value' => variable_get('paging_api_views_enabled_views', array()),
  );

  return system_settings_form($form);
}